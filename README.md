LFG is an application to plan workouts.

Exercises are organized by muscle group.

Users create custom workouts:
 - name your custom workout
 - add exercises with a (+) button or by drag and drop
 - reorder exercises within a workout using drag and drop
 - remove unwanted exercises with a (-) button

Remove unwanted workouts with a (-) button

Add workouts to a specific day to plan out your week

Print a summary to bring your workouts to the gym

Users are able to set up login information to save their own list of workouts