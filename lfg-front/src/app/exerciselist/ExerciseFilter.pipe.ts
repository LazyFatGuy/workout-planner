import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'exerciseFilter',
  pure: false
})

export class ExerciseFilterPipe implements PipeTransform {
  transform(exercises: any[], searchExercise: string, searchEquipment: string) {
    if (exercises && exercises.length) {
      return exercises.filter(exercise => {
        if (searchExercise && exercise.name.toLowerCase().indexOf(searchExercise.toLowerCase()) === -1) {
          return false;
        }
        if (searchEquipment && exercise.eqId.id.toString().toLowerCase().indexOf(searchEquipment.toString().toLowerCase()) === -1) {
          console.log('eq id test');
          return false;
        }
        return true;
      });
    } else {
      return exercises;
    }
  }
}
