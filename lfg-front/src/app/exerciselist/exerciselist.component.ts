import { Component, OnInit } from '@angular/core';
import {ExerciselistService} from '../exerciselist.service';
import {EquipmentService} from '../equipment.service';

@Component({
  selector: 'app-exerciselist',
  templateUrl: './exerciselist.component.html',
  styleUrls: ['./exerciselist.component.css']
})
export class ExerciselistComponent implements OnInit {
  public exercises: Array<any>;
  private equipment: Array<any>;
  private exEq: Array<any>;
  private eqId: Array<any>;
  private exId: Array<any>;

  searchExercise: string;
  searchEquipment: string;
  constructor(private ExerciseService: ExerciselistService, private equipmentService: EquipmentService) {
  }

  ngOnInit() {
    this.ExerciseService.getAllExercises().subscribe(data => {
      this.exercises = data;
    });
    this.equipmentService.getAllEquipment().subscribe(data => {
      this.equipment = data;
    });
    this.ExerciseService.getExEq(this.eqId, this.exId).subscribe(data => {
      this.exEq = data;
    });
  }

}
