import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WorkoutTemplateService {

  constructor(private http: HttpClient) { }
  getAllWt(): Observable<any> {
    return this.http.get('https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/wt');
  }
  createNewWt(wt): Observable<any> {
    return this.http.post( 'https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/wt/create', wt);
  }
  removeWt(wt): Observable<any> {
    return this.http.post( 'https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/wt/remove', wt);
  }
  moveExWt(wt, oldIndex, newIndex): Observable<any> {
    return this.http.post( 'https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/exwt/move/' + oldIndex + '/' + newIndex, wt);
  }
  getAllExWt(): Observable<any> {
    return this.http.get('https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/exwt');
  }
  insertNewExWt(exerciseId, newIndex, wt): Observable<any> {
    return this.http.post( 'https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/exwt/create/' + exerciseId + '/' + newIndex, wt);
  }
  createNewExWt(exwt): Observable<any> {
    return this.http.post( 'https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/exwt/create/', exwt);
  }
  removeExWt(exwt): Observable<any> {
    return this.http.post( 'https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/exwt/remove', exwt);
  }
  moveExWtLater(exwt): Observable<any> {
    return this.http.post( 'https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/exwt/later', exwt);
  }
  moveExWtEarlier(exwt): Observable<any> {
    return this.http.post( 'https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/exwt/earlier', exwt);
  }
}
