import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ScheduledWorkoutService {
  constructor(private http: HttpClient) { }
  getAllDates(): Observable<any> {
    return this.http.get('https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/date');
  }
  scheduleWorkout(wtId, date): Observable<any> {
    return this.http.post( 'https://lfg-back-thankful-possum.apps.pcf.pcfhost.com//sw/duplicate/' + wtId, date);
  }
  getAllSw(): Observable<any> {
    return this.http.get('https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/sw');
  }
  removeSw(sw): Observable<any> {
    return this.http.post( 'https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/sw/remove', sw);
  }
  getAllExSw(): Observable<any> {
    return this.http.get('https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/exsw');
  }
  removeExSw(exSw): Observable<any> {
    return this.http.post( 'https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/exsw/remove', exSw);
  }
}
