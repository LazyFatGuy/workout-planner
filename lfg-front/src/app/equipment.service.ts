import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EquipmentService {

  constructor(private http: HttpClient) { }
  getAllEquipment(): Observable<any> {
    return this.http.get('https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/equipment');
  }
}
