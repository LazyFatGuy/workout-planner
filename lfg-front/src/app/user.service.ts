import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  getAllUsers(): Observable<any> {
    return this.http.get('https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/user');
  }

  loginCheck(info): Observable<any> {
    return this.http.post( 'https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/login', info);
  }
  createNewUser(user): Observable<any> {
    return this.http.post( 'https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/user/create', user);
  }
}
