import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {UserService} from '../user.service';
import {ContextService} from '../context.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
  userForm: FormGroup;
  constructor(private userService: UserService, private contextService: ContextService) { }

  ngOnInit() {
    this.userForm = new FormGroup({
      username: new FormControl(),
      password: new FormControl(),
      email: new FormControl()
    });
  }

  postUser(): void {
    const user = {
      username: this.userForm.controls.username.value,
      password: this.userForm.controls.password.value,
      email: this.userForm.controls.email.value
    };
    this.userService.createNewUser(user).subscribe( data => {
      this.contextService.user = data;
    });
    this.userForm.reset();
  }
}
