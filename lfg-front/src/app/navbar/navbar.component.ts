import { Component, OnInit } from '@angular/core';
import {ContextService} from '../context.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public contextService: ContextService) { }

  ngOnInit() {
  }
  signOut() {
    this.contextService.user = {id: 0};
  }
}
