import { Component } from '@angular/core';
import {ScheduledWorkoutService} from './scheduled-workout.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'lfg-front';
  constructor() { }
}

