import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContextService {
  // user: any = {id: 0};
  // Revert to the line above in a production environment
  user: any = {id: 1, username: 'user', password: 'pass', email: 'test@test.com'};
  constructor() { }
}
