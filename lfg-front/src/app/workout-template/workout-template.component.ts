import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ContextService} from '../context.service';
import {WorkoutTemplateService} from '../workout-template.service';
import {ExerciselistService} from '../exerciselist.service';
import {CdkDragDrop} from '@angular/cdk/drag-drop';
import {ScheduledWorkoutService} from '../scheduled-workout.service';

@Component({
  selector: 'app-workout-template',
  templateUrl: './workout-template.component.html',
  styleUrls: ['./workout-template.component.css']
})
export class WorkoutTemplateComponent implements OnInit {
  public wtlist: Array<any>;
  public exwtlist: Array<any>;
  public exerciselist: Array<any>;
  public muscleGroups: Array<any>;
  public muExList: Array<any>;
  public workoutid = 0;
  public wtForm: FormGroup;
  constructor(
    private wtService: WorkoutTemplateService,
    private contextService: ContextService,
    private exerciseService: ExerciselistService) { }

  ngOnInit() {
    this.wtForm = new FormGroup({
      name: new FormControl()
    });
    this.wtService.getAllWt().subscribe(data => {
      this.wtlist = data;
    });
    this.wtService.getAllExWt().subscribe(data => {
      this.exwtlist = data;
    });
    this.exerciseService.getAllExercises().subscribe(data => {
      this.exerciselist = data;
    });
    this.exerciseService.getAllMuEx().subscribe(data => {
      this.muExList = data;
    });
    this.exerciseService.getAllMuscleGroups().subscribe(data => {
      this.muscleGroups = data;
    });
  }

  postWt(): void {
    const wt = {
      name: this.wtForm.controls.name.value,
      userId: this.contextService.user.id ? {id: this.contextService.user.id} : null
    };
    console.log(this.contextService.user);
    this.wtService.createNewWt(wt).subscribe( data => {
      this.wtlist = data;
    });
    this.wtForm.reset();
  }
  selectWt(wtid): void {
    this.workoutid = wtid;
  }
  addExercise(exerciseid): void {
    if (this.workoutid > 0) {
      const exwt = {
        exId: exerciseid ? {id: exerciseid} : null,
        wtId: this.workoutid ? {id: this.workoutid} : null,
      };
      this.wtService.createNewExWt(exwt).subscribe(data => {
        this.exwtlist = data;
        this.wtService.getAllExWt().subscribe(data1 => {
          this.exwtlist = data1;
        });
      });
    }
    console.log(exerciseid);
  }

  removeWorkout(workout): void {
    workout.userId = null;
    this.wtService.removeWt(workout).subscribe( data => {
      this.wtlist = data;
    });
  }
  removeExercise(exwt): void {
    this.wtService.removeExWt(exwt).subscribe(data => {
      this.exwtlist = data;
    });
  }

  doExerciseLater(exwt): void {
    this.wtService.moveExWtLater(exwt).subscribe(data => {
      this.exwtlist = data;
    });
  }
  doExerciseEarlier(exwt): void {
    this.wtService.moveExWtEarlier(exwt).subscribe(data => {
      this.exwtlist = data;
    });
  }
  drop(event: CdkDragDrop<string[]>, wt) {
    console.log(event);
    console.log(wt);
    console.log(event.previousContainer.id);
    if (event.previousContainer.id === 'exercise-card') {
      const exerciseId = event.item.element.nativeElement.id.replace('exercise-', '' );
      this.addExercise(exerciseId);
    }
    console.log(event.previousContainer.id);
    if (event.previousContainer.id.includes('workout-')) {
      console.log('wt');
      console.log(wt);
      const prev = +(event.previousIndex + 1);
      const next = +(event.currentIndex + 1);
      console.log(event.previousIndex + 1);
      console.log(event.currentIndex + 1);
      this.wtService.moveExWt(wt, prev, next).subscribe(data => {
        this.exwtlist = data;
      });
    }
  }
}
