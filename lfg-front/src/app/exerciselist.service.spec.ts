import { TestBed } from '@angular/core/testing';

import { ExerciselistService } from './exerciselist.service';

describe('ExerciselistService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExerciselistService = TestBed.get(ExerciselistService);
    expect(service).toBeTruthy();
  });
});
