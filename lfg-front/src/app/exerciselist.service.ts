import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExerciselistService {

  constructor(private http: HttpClient) { }
    getAllExercises(): Observable<any> {
      return this.http.get('https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/exercises');
    }
    getExEq(id1, id2): Observable<any> {
      return this.http.get('https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/exercises/' + id1 + '/' + id2);
    }
    getAllMuEx(): Observable<any> {
      return this.http.get('https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/MusclePivot');
    }
    getAllMuscleGroups(): Observable<any> {
      return this.http.get('https://lfg-back-thankful-possum.apps.pcf.pcfhost.com/muscleGroup');
    }
}
