import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduledWorkoutComponent } from './scheduled-workout.component';

describe('ScheduledWorkoutComponent', () => {
  let component: ScheduledWorkoutComponent;
  let fixture: ComponentFixture<ScheduledWorkoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduledWorkoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduledWorkoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
