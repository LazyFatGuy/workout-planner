import { Component, OnInit } from '@angular/core';
import {ScheduledWorkoutService} from '../scheduled-workout.service';
import {WorkoutTemplateService} from '../workout-template.service';
import {CdkDragDrop} from '@angular/cdk/drag-drop';
import {FormControl, FormGroup} from '@angular/forms';


@Component({
  selector: 'app-scheduled-workout',
  templateUrl: './scheduled-workout.component.html',
  styleUrls: ['./scheduled-workout.component.css']
})
export class ScheduledWorkoutComponent implements OnInit {
public dateList: Array<any>;
public wtList: Array<any>;
public swList: Array<any>;
public exSwList: Array<any>;
public dropdowns: FormGroup;
  constructor(
    private swService: ScheduledWorkoutService,
    private wtService: WorkoutTemplateService) { }
  ngOnInit() {
    this.swService.getAllDates().subscribe(data => {
      this.dateList = data;
    });
    this.wtService.getAllWt().subscribe(data => {
      this.wtList = data;
    });
    this.swService.getAllSw().subscribe(data => {
      this.swList = data;
    });
    this.swService.getAllExSw().subscribe(data => {
      this.exSwList = data;
    });
    this.dropdowns = new FormGroup({
      dropdown: new FormControl()
    });
  }
  scheduleWorkout(wtId, date) {
    this.swService.scheduleWorkout(wtId, date).subscribe( data => {
      this.swList = data;
      this.swService.getAllExSw().subscribe(data2 => {
        this.exSwList = data2;
      });
    });
    this.dropdowns.reset();
  }
  cancelWorkout(sw) {
    this.swService.removeSw(sw).subscribe( data => {
      this.swList = data;
    });
  }
  refresh() {
    this.wtService.getAllWt().subscribe(data => {
      this.wtList = data;
    });
  }
  removeExercise(exsw): void {
    this.swService.removeExSw(exsw).subscribe(data => {
      this.exSwList = data;
    });
  }

}
