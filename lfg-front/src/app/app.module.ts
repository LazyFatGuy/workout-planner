import { BrowserModule } from '@angular/platform-browser';
import {NgModule, PipeTransform} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatDialogModule, MatButtonModule, MatCardModule,
  MatFormFieldModule, MatInputModule,
  MatSelectModule, MatCheckboxModule
} from '@angular/material';
import {MatGridListModule} from '@angular/material/grid-list';

import { ExerciselistComponent } from './exerciselist/exerciselist.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NewUserComponent } from './new-user/new-user.component';
import { WorkoutTemplateComponent } from './workout-template/workout-template.component';
import {ExerciseFilterPipe} from './exerciselist/ExerciseFilter.pipe';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatExpansionModule} from '@angular/material/expansion';
import { ScheduledWorkoutComponent } from './scheduled-workout/scheduled-workout.component';
import {MatIconModule} from '@angular/material/icon';
import {NgxPrintModule} from 'ngx-print';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ExerciselistComponent,
    NavbarComponent,
    NewUserComponent,
    WorkoutTemplateComponent,
    ExerciseFilterPipe,
    ScheduledWorkoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgxPrintModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatCardModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatGridListModule,
    MatDialogModule,
    MatCheckboxModule,
    AppRoutingModule,
    DragDropModule,
    MatExpansionModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
