import {Component, HostListener, OnInit} from '@angular/core';
import {UserService} from '../user.service';
import {FormControl, FormGroup} from '@angular/forms';
import {ContextService} from '../context.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login: FormGroup;
  userLoginInfo: any[];
  invalidSignin: boolean;
  constructor(private userService: UserService,
              private contextService: ContextService,
              public router: Router) { }

  ngOnInit() {
    this.login = new FormGroup({
      username: new FormControl(),
      password: new FormControl()
    });
    this.invalidSignin = false;
  }

  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.submitLogin();
    }
  }

  submitLogin() {
    console.log('clicked');
    const loginInfo = {
      username: this.login.controls.username.value,
      password: this.login.controls.password.value
    };
    this.userService.loginCheck(loginInfo).subscribe( data => {
      this.userLoginInfo = data;
      if (this.userLoginInfo !== null) {
        this.contextService.user = this.userLoginInfo;
        this.router.navigateByUrl('/');
      } else {
        this.invalidSignin = true;
      }
    });
    this.login.reset();
  }
}
