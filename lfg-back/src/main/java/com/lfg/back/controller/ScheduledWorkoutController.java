package com.lfg.back.controller;

import com.lfg.back.model.*;
import com.lfg.back.repository.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
public class ScheduledWorkoutController {
    private ScheduledWorkoutRepository swRepo;
    private Ex_Sw_PivotRepository exSwRepo;
    private ExerciseRepository exRepo;
    private DateRepository dateRepo;
    private Ex_Wt_PivotRepository exWtRepo;
    private Workout_TemplateRepository wtRepo;

    public ScheduledWorkoutController (ScheduledWorkoutRepository swRepo,
                                      Ex_Sw_PivotRepository exSwRepo,
                                      ExerciseRepository exRepo,
                                      DateRepository dateRepo,
                                       Ex_Wt_PivotRepository exWtRepo,
                                       Workout_TemplateRepository wtRepo)
    {
        this.swRepo = swRepo;
        this.exSwRepo = exSwRepo;
        this.exRepo = exRepo;
        this.dateRepo = dateRepo;
        this.exWtRepo = exWtRepo;
        this.wtRepo = wtRepo;
    }

    @GetMapping("/date")
    public List<WtDate> dateList(){
        return (List<WtDate>)dateRepo.findAll();
    }

    @GetMapping("/sw")
    public List<Scheduled_Workout> swList(){
        return (List<Scheduled_Workout>)swRepo.findAll();
    }

    @PostMapping("/sw/duplicate/{wtId}")
    public List<Scheduled_Workout> createWt(@RequestBody WtDate date, @PathVariable Long wtId){
        List<Ex_Wt_Pivot> exWtList = exWtRepo.findAllByWtId(wtRepo.findById(wtId).get());
        Scheduled_Workout sw = new Scheduled_Workout();
        sw.setDateId(date);
        sw.setNote(wtRepo.findById(wtId).get().getName());
        sw = swRepo.save(sw);
        for (Ex_Wt_Pivot exWt : exWtList) {
            Ex_Sw_Pivot exSw = new Ex_Sw_Pivot();
            exSw.setExId(exWt.getExId());
            exSw.setExerciseOrder(exWt.getExerciseOrder());
            exSw.setSwId(sw);
            exSwRepo.save(exSw);
        }
        return swList();
    }
    @PostMapping("/sw/remove")
    public List<Scheduled_Workout> removeWt(@RequestBody Scheduled_Workout sw){
        List<Ex_Sw_Pivot> exercises = exSwRepo.findAllBySwId(sw);
        for (Ex_Sw_Pivot exSw : exercises) {
            exSwRepo.delete(exSw);
        }
        swRepo.delete(sw);
        return swList();
    }
    @GetMapping("/exsw")
    public List<Ex_Sw_Pivot> exSwList(){
        return (List<Ex_Sw_Pivot>)exSwRepo.findAll();
    }
    @PostMapping("/exsw/remove")
    public List<Ex_Sw_Pivot> removeExSw(@RequestBody Ex_Sw_Pivot exSw){
        exSwRepo.delete(exSw);
        return exSwList();
    }
}
