package com.lfg.back.controller;

import com.lfg.back.repository.*;
import com.lfg.back.model.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class WorkoutTemplateController {
    private Workout_TemplateRepository Wt_repo;
    private Ex_Wt_PivotRepository Ex_Wt_repo;
    private ExerciseRepository Ex_repo;

    public WorkoutTemplateController (Workout_TemplateRepository Wt_repo,
                                      Ex_Wt_PivotRepository Ex_Wt_repo,
                                      ExerciseRepository Exercise_repo)
    {
        this.Wt_repo = Wt_repo;
        this.Ex_Wt_repo = Ex_Wt_repo;
        this.Ex_repo = Exercise_repo;
    }

    @GetMapping("/wt")
    public List<Workout_Template> wtList(){
        return (List<Workout_Template>)Wt_repo.findAll();
    }
    @PostMapping("/wt/create")
    public List<Workout_Template> createWt(@RequestBody Workout_Template wt){
        Wt_repo.save(wt);
        return wtList();
    }
    @PostMapping("/wt/remove")
    public List<Workout_Template> removeWt(@RequestBody Workout_Template wt){
        List<Ex_Wt_Pivot> exercises = Ex_Wt_repo.findAllByWtId(wt);
        for (int i = 0; i < exercises.size(); i++) {
            removeExWt(exercises.get(i));
        }
        Wt_repo.delete(wt);
        return wtList();
    }
    @GetMapping("/exwt")
    public List<Ex_Wt_Pivot> exWtList(){
        return (List<Ex_Wt_Pivot>)Ex_Wt_repo.findAllOrderByWtIdExerciseOrder();
    }
    @PostMapping("/exwt/create")
    public List<Ex_Wt_Pivot> createExWt(@RequestBody Ex_Wt_Pivot exWt){
        List<Ex_Wt_Pivot> exercises = Ex_Wt_repo.findAllOrderByWtIdExerciseOrder();
        int workoutOrder = 1;
        for (int i = 0; i < exercises.size(); i++) {
            if (exercises.get(i).getWtId().getId()==exWt.getWtId().getId()) {
                if (workoutOrder <= exercises.get(i).getExerciseOrder()) {
                    workoutOrder = exercises.get(i).getExerciseOrder() + 1;
                }
            }
        }
        exWt.setExerciseOrder(workoutOrder);
        Ex_Wt_repo.save(exWt);
        return exWtList();
    }
    @PostMapping("/exwt/remove")
    public List<Ex_Wt_Pivot> removeExWt(@RequestBody Ex_Wt_Pivot exWt){
        List<Ex_Wt_Pivot> exercises = Ex_Wt_repo.findAllByWtId(exWt.getWtId());
        int workoutOrder = exWt.getExerciseOrder();
        for (int i = 0; i < exercises.size(); i++) {
            if (exercises.get(i).getWtId().getId()==exWt.getWtId().getId()) {
                if (workoutOrder < exercises.get(i).getExerciseOrder()) {
                    exercises.get(i).setExerciseOrder(exercises.get(i).getExerciseOrder()-1);
                }
            }
        }
        Ex_Wt_repo.delete(exWt);
        return exWtList();
    }
    @PostMapping("/exwt/move/{oldIndex}/{newIndex}")
    public List<Ex_Wt_Pivot> moveExWt(@RequestBody Workout_Template workout, @PathVariable int oldIndex, @PathVariable int newIndex){
        List<Ex_Wt_Pivot> exercises = Ex_Wt_repo.findAllByWtIdOrderByExerciseOrder(workout);
        if (oldIndex < newIndex) {
            for (int i = oldIndex-1; i < newIndex; i++) {
               if (i == oldIndex-1) {
                    exercises.get(i).setExerciseOrder(newIndex);
                    Ex_Wt_repo.save(exercises.get(i));
                } else {
                    exercises.get(i).setExerciseOrder(i);
                    Ex_Wt_repo.save(exercises.get(i));
                }
            }
        }
        if (newIndex < oldIndex) {
            for (int i = newIndex-1; i < oldIndex; i++) {
                if (i == oldIndex-1) {
                    exercises.get(i).setExerciseOrder(newIndex);
                    Ex_Wt_repo.save(exercises.get(i));
                } else {
                    exercises.get(i).setExerciseOrder(i+2);
                    Ex_Wt_repo.save(exercises.get(i));
                }
            }
        }
        return exWtList();
    }
    @PostMapping("/exwt/later")
    public List<Ex_Wt_Pivot> moveExWtLater(@RequestBody Ex_Wt_Pivot exWt){
        List<Ex_Wt_Pivot> exercises = Ex_Wt_repo.findAllByWtId(exWt.getWtId());
        int workoutOrder = exWt.getExerciseOrder();
        for (int i = 0; i < exercises.size(); i++) {
            if (exercises.get(i).getExerciseOrder() == workoutOrder+1) {
                exercises.get(i).setExerciseOrder(workoutOrder);
                exWt.setExerciseOrder(workoutOrder+1);
                Ex_Wt_repo.save(exWt);
            }
        }
        return exWtList();
    }
    @PostMapping("/exwt/earlier")
    public List<Ex_Wt_Pivot> moveExWtEarlier(@RequestBody Ex_Wt_Pivot exWt){
        List<Ex_Wt_Pivot> exercises = Ex_Wt_repo.findAllByWtId(exWt.getWtId());
        int workoutOrder = exWt.getExerciseOrder();
        for (int i = 0; i < exercises.size(); i++) {
            if (exercises.get(i).getExerciseOrder() == workoutOrder-1) {
                exercises.get(i).setExerciseOrder(workoutOrder);
                exWt.setExerciseOrder(workoutOrder-1);
                Ex_Wt_repo.save(exWt);
            }
        }
        return exWtList();
    }
}
