package com.lfg.back.controller;

import com.lfg.back.model.User;
import com.lfg.back.repository.UserRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class UserController {
    private UserRepository repo;
    public UserController(UserRepository repo){this.repo =repo;}
    @GetMapping("/user")
    public List<User> userList(){

        List<User> list = (List<User>)repo.findAll();
        System.out.println(list.size());
        return list;
    }
    @PostMapping("/user/create")
    public User createItem(@RequestBody User user){
        repo.save(user);
        return user;
    }
    @PostMapping("/login")
    public User createEvent(@RequestBody  User loginInfo) {
        System.out.println(loginInfo.getUsername() +" "+loginInfo.getPassword());
        List<User> user = repo.findAllByUsername(loginInfo.getUsername());
        for (int i = 0; i < user.size(); i++) {
            if (user.get(i).getPassword().equals(loginInfo.getPassword())) {
                return user.get(i);
            }
        }
        return null;
    }
}
