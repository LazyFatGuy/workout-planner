package com.lfg.back.controller;


import com.lfg.back.model.Eq_Ex_Pivot;
import com.lfg.back.model.Equipment;
import com.lfg.back.model.Exercise;
import com.lfg.back.repository.Eq_Ex;
import com.lfg.back.repository.ExerciseRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class ExerciseController {
    private ExerciseRepository exerciseRepo;
    private Eq_Ex eq_exRepo;

    public ExerciseController(ExerciseRepository exerciseRepo, Eq_Ex eq_exRepo){this.exerciseRepo = exerciseRepo;
    this.eq_exRepo= eq_exRepo;}

    @GetMapping("/exercises")
    public List<Exercise> exerciseList() {
        return (List<Exercise>) exerciseRepo.findAll();
    }
    @GetMapping("/eqEx/{EqId}/{ExId}")
    public List<Eq_Ex_Pivot> eqExPivots(@PathVariable Equipment EqId, @PathVariable Exercise ExId) {
        return eq_exRepo.getAllByEqIdAndExId(EqId, ExId);
    }
}
