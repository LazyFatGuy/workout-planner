package com.lfg.back.controller;

import com.lfg.back.model.Equipment;
import com.lfg.back.repository.EquipmentRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class EquipmentController {
    private EquipmentRepository equipmentRepo;

    public EquipmentController(EquipmentRepository equipmentRepo){this.equipmentRepo = equipmentRepo;}

    @GetMapping("/equipment")
    public List<Equipment> equipmentList() {
        return (List<Equipment>) equipmentRepo.findAll();
    }
}
