package com.lfg.back.controller;

import com.lfg.back.model.Mu_Ex_Pivot;
import com.lfg.back.model.Muscle_Group;
import com.lfg.back.repository.Mu_ExRepository;
import com.lfg.back.repository.Muscle_GroupRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class MuscleGroupController {
    private Muscle_GroupRepository muscleRepo;
    private Mu_ExRepository mu_exRepo;

    public MuscleGroupController(Muscle_GroupRepository muscleRepo, Mu_ExRepository mu_exRepo){this.muscleRepo = muscleRepo;
    this.mu_exRepo = mu_exRepo;}

    @GetMapping("/muscleGroup")
    public List<Muscle_Group> muscleGroupList() {
        return (List<Muscle_Group>) muscleRepo.findAll();
    }

    @GetMapping("/MusclePivot")
    public List<Mu_Ex_Pivot> muExList() {return (List<Mu_Ex_Pivot>) mu_exRepo.findAll();}
}

