package com.lfg.back.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Equipment {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @Column
    private String name;
}
