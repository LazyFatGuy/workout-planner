package com.lfg.back.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Ex_Sw_Pivot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "Exercise", referencedColumnName = "ID")
    private Exercise exId;

    @ManyToOne
    @JoinColumn(name = "Scheduled_workout", referencedColumnName = "ID")
    private Scheduled_Workout swId;

    @Column
    private String note;

    @Column
    private int exerciseOrder;
}
