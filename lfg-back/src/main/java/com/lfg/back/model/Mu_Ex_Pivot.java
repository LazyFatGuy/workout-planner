package com.lfg.back.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Mu_Ex_Pivot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name="Exercise", referencedColumnName = "id")
    private Exercise exId;

    @ManyToOne
    @JoinColumn(name="Muscle_Group", referencedColumnName = "id")
    private Equipment muId;
}
