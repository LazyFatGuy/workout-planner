package com.lfg.back.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Ex_Wt_Pivot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "Exercise", referencedColumnName = "ID")
    private Exercise exId;

    @ManyToOne
    @JoinColumn(name = "Workout_template", referencedColumnName = "ID")
    private Workout_Template wtId;

    @Column
    private int exerciseOrder;

}
