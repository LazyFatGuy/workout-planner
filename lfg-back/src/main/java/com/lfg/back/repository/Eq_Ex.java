package com.lfg.back.repository;

import com.lfg.back.model.Eq_Ex_Pivot;
import com.lfg.back.model.Equipment;
import com.lfg.back.model.Exercise;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface Eq_Ex extends CrudRepository<Eq_Ex_Pivot, Long> {
    List<Eq_Ex_Pivot> getAllByEqIdAndExId(Equipment EqId, Exercise ExId);
}
