package com.lfg.back.repository;

import com.lfg.back.model.Scheduled_Workout;
import org.springframework.data.repository.CrudRepository;

public interface ScheduledWorkoutRepository extends CrudRepository<Scheduled_Workout, Long> {
}
