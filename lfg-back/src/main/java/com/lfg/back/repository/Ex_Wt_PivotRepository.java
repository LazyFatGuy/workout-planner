package com.lfg.back.repository;

import com.lfg.back.model.Ex_Wt_Pivot;
import com.lfg.back.model.Workout_Template;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface Ex_Wt_PivotRepository extends CrudRepository<Ex_Wt_Pivot, Long> {
    @Query(nativeQuery = true,
            value = "SELECT * FROM ex_wt_pivot ORDER BY workout_template, exercise_order;")
    List<Ex_Wt_Pivot> findAllOrderByWtIdExerciseOrder();

    List<Ex_Wt_Pivot> findAllByWtId(Workout_Template wtId);
    List<Ex_Wt_Pivot> findAllByWtIdOrderByExerciseOrder(Workout_Template wtId);
}
