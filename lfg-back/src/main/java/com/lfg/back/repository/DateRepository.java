package com.lfg.back.repository;

import com.lfg.back.model.WtDate;
import org.springframework.data.repository.CrudRepository;

public interface DateRepository extends CrudRepository<WtDate, Long> {
}
