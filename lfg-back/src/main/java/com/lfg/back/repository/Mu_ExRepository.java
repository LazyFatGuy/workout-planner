package com.lfg.back.repository;

import com.lfg.back.model.Mu_Ex_Pivot;
import org.springframework.data.repository.CrudRepository;

public interface Mu_ExRepository extends CrudRepository<Mu_Ex_Pivot, Long> {
}
