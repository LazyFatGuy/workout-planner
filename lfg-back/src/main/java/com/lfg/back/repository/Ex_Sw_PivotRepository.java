package com.lfg.back.repository;

import com.lfg.back.model.Ex_Sw_Pivot;
import com.lfg.back.model.Scheduled_Workout;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface Ex_Sw_PivotRepository extends CrudRepository<Ex_Sw_Pivot, Long> {
    List<Ex_Sw_Pivot> findAllBySwId(Scheduled_Workout swId);
}
