package com.lfg.back.repository;

import com.lfg.back.model.Workout_Template;
import org.springframework.data.repository.CrudRepository;

public interface Workout_TemplateRepository extends CrudRepository<Workout_Template, Long> {
}
