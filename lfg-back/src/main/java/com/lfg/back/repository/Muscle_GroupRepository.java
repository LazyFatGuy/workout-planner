package com.lfg.back.repository;

import com.lfg.back.model.Muscle_Group;
import org.springframework.data.repository.CrudRepository;

public interface Muscle_GroupRepository extends CrudRepository<Muscle_Group, Long> {
}
